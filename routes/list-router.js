const { Router } = require('express');
const User = require('../models/user');
const WatchedListItem = require('../models/watched-list');
const MainListItem = require('../models/main-list');

const router = Router();

// добавить фильм в список просмотренного
router.post('/add-to-watched', async (req, res) => {
    try {
        const { userId, item } = req.body;

        if(item !== null) {

            await User.findByIdAndUpdate(userId, {$pull: {mainList: item._id}});
            await MainListItem.findByIdAndDelete(item._id);
    
            delete item._id;
    
            const newItem = new WatchedListItem(item);
            const addedItem = await newItem.save();
    
            await User.findByIdAndUpdate(userId, {$push: {watchedList: { $each:[addedItem._id], $position:0 }}});
    
            res.status(200).json({message: "Фильм добавлен в список просмотренного", item: addedItem});
        }


    } catch (e) {
        res.status(500).json({message: "При добавлении элемента что то пошло не так. " + e});
    }
});

// получить весь список просмотренных фильмов
router.post('/get-all-watched', async (req, res) => {
    try {
        const { owner } = req.body;

        const user = await User.findById(owner);
        const list = await WatchedListItem.find({_id: {$in: user.watchedList}});

        const sortedList = user.watchedList.map(itemId => {
            return list.find(item => item._id.toString() === itemId.toString());
        });

        res.json(sortedList);

    } catch (e) {
        res.status(500).json({message: "При получении элементов что то пошло не так. " + e});
    }
});

// удаление фильма из основного списка
router.post('/delete-from-main', async (req, res) => {
    try {
        const { owner, deletedId } = req.body;

        await User.findByIdAndUpdate(owner, {$pull: {mainList: deletedId}});
        await MainListItem.findByIdAndDelete(deletedId);

        res.status(200).json({message: "Список обновлён"});
    } catch (e) {
        res.status(500).json({message: "При удалении элемента что-то пошло не так. " + e});
    }
});

// получение всего списка фильмов
router.post('/get-main-list', async (req, res) => {
    try {
        const { owner } = req.body;

        const user = await User.findById(owner);
        const list = await MainListItem.find({_id: {$in: user.mainList}});

        const sortedList = user.mainList.map(itemId => {
            return list.find(item => item._id.toString() === itemId.toString());
        });

        res.json(sortedList);

    } catch (e) {
        res.status(500).json({message: "При получении элементов что то пошло не так. " + e});
    }
});

// добавить фильм в основной список
router.post('/add-to-main-list', async (req, res) => {
    try {
        const { userId, item } = req.body;

        if(item !== null) {

            if (!userId) {
                res.status(500).json({message: "При добавлении элемента что то пошло не так. " + e});
            }
    
            const isExists = await MainListItem.findById(item._id);
    
            if(isExists) {
                return;
            }
    
            const newItem = new MainListItem(item);
    
            const addedItem = await newItem.save();
    
            await User.findByIdAndUpdate(userId, {$push: {mainList: { $each:[addedItem._id], $position:0 }}});
    
            res.status(200).json({addedItemId: addedItem._id, message: "Фильм добавлен в список", item: addedItem});
        }
    } catch (e) {
        res.status(500).json({message: "При добавлении фильма, что-то пошло не так. " + e});
    }
});

// изменение порядка массива фильмов
router.post('/reorder-main-list', async (req, res) => {
    try {
        const { owner, list } = req.body;

        const isNull = list.some(item => item === null);

        if(!isNull) {

            await User.findByIdAndUpdate(owner, {$set: {mainList: list}});
    
            res.status(200).json({message: "Список обновлён"});
        }
    } catch (e) {
        res.status(500).json({message: "При переопределении списка, что-то пошло не так. " + e});
    }
});

module.exports = router;