const { Router } = require('express');
const bcrypt = require('bcryptjs');
const User = require('../models/user');

const router = Router();

router.post('/signup', async (req, res) => {
    try {
        const { email, password, login } = req.body;

        const candidateByEmail = await User.findOne({ email });

        if(candidateByEmail) {
            return res.status(400).json({status: 400, error: 'Этот email уже занят', message: "Такой пользовательно уже существует."});
        }

        const candidateByLogin = await User.findOne({ login });

        if(candidateByLogin) {
            return res.status(400).json({status: 400, error: 'Этот логин уже занят', message: "Такой пользовательно уже существует."});
        }

        const hashedPassword = await bcrypt.hash(password, 12);

        const user = new User({
            email,
            login,
            password: hashedPassword
        });

        await user.save();

        res.status(200).json({status: 200, message: "Пользователь создан"});

    } catch (e) {
        res.status(500).json({message: "Что-то пошло не так, попробуйте снова. " + e});
    }
});

router.post('/signin', async (req, res) => {
    try {
        const { email, password } = req.body;

        if(email.length === 0 || password.length === 0) {
            return res.status(400).json({status: 400, message: "Неверный логин или пароль, попробуйте снова."});
        }

        let user = await User.findOne({ $or: [{email: email}, {login: email}] });

        if(!user) {
            return res.status(400).json({status: 400, error: "login", message: "Неверный логин или пароль, попробуйте снова."});
        }

        const isMatch = bcrypt.compare(password, user.password);

        isMatch.then(data => {
            if(data) {
                res.json({ 
                    status: 200,
                    userId: user.id,
                    userEmail: user.email,
                    login: user.login
                });
            } else {
                return res.status(400).json({status: 400, error: "pass", message: "Неверный логин или пароль, попробуйте снова."});
            }
        })
    } catch (e) {
        res.status(500).json({message: "Что-то пошло не так, попробуйте снова."});
    }
});

router.post('/authorize', async (req, res) => {
    try {
        const { userId } = req.body;

        const user = await User.findById(userId);

        if(!user) {
            return res.status(400).json({message: "Пользователь не найден."});
        }

        res.json({
            status: 200,
            userId: user.id,
            userEmail: user.email,
            login: user.login
        });

    } catch (e) {
        res.status(500).json({message: "Что-то пошло не так, попробуйте снова." + e});
    }
});

module.exports = router;