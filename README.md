## notefy

Service for checking watched movies

Install all dependencies `npm run install_dependencies`

Run project `npm run dev`


Check app [https://notefy-dev.herokuapp.com/](https://notefy-dev.herokuapp.com/)
