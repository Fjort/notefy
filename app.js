const express = require('express');
const config = require('config');
const mongoose = require('mongoose');
const authRoutes = require('./routes/auth-router');
const itemsRoutes = require('./routes/list-router');
const bodyParser = require('body-parser');

const app = express();
const PORT = config.get('port') || 5000;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use('/api/auth', authRoutes);
app.use('/api/lists', itemsRoutes);

async function start() {
    try {
        await mongoose.connect(config.get('mongoURI'), {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true,
            useFindAndModify: false
        });

        app.listen(PORT, (req, res) => {
            console.log("Server started on port " + PORT)
        });
    } catch (e) {
        console.log('Server error: ' + e);
        process.exit(1);
    }
}

start();


