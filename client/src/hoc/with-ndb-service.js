import React from 'react';
import { NDBConsumer } from '../context';

const withNDBService = () => Wrapped => {
    return props => {
        return (
            <NDBConsumer>
                {
                    ndbService => {
                        return <Wrapped  {...props} ndbService={ndbService}/>
                    }
                }
            </NDBConsumer>
        )
    }
    
}

export default withNDBService;