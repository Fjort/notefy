import withTmDBService from './with-tmdb-service';
import withNDBService from './with-ndb-service';

export {
    withTmDBService,
    withNDBService
}