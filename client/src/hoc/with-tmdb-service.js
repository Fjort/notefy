import React from 'react';
import { TmDBConsumer } from '../context';

const withTmDBService = () => Wrapped => {
    return props => {
        return (
            <TmDBConsumer>
                {
                    tmdbService => {
                        return <Wrapped  {...props} tmdbService={tmdbService}/>
                    }
                }
            </TmDBConsumer>
        )
    }
    
}

export default withTmDBService;