export default class NotefyDB {

    signUp = async (formData) => {
        const formObj = {};

        formData.forEach((value, key) => formObj[key] = value);

        const request = {
            method: 'POST',
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(formObj)
        }

        const resp = await fetch('/api/auth/signup', request);

        return resp.json();
    }

    signIn = async (formData) => {
        const formObj = {};

        formData.forEach((value, key) => formObj[key] = value);

        const request = {
            method: 'POST',
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(formObj)
        }

        const resp = await fetch('/api/auth/signin', request);

        return resp.json();
    }

    authorize = async (userId) => {
        const request = {
            method: 'POST',
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({userId})
        }

        const resp = await fetch('/api/auth/authorize', request);

        return await resp.json();
    }

    getMainList = async (userId) => {
        const request = {
            method: 'POST',
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({owner: userId})
        }

        const resp = await fetch('/api/lists/get-main-list', request);

        return await resp.json();
    }

    deleteFromMainList = async (userId, deletedId) => {
        const obj = {
            owner: userId,
            deletedId: deletedId
        }

        const request = {
            method: 'POST',
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(obj)
        }

        const resp = await fetch('/api/lists/delete-from-main', request);

        return await resp.json();
    }

    addToMainList = async (body) => {
        const request = {
            method: 'POST',
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(body)
        }

        const resp = await fetch('/api/lists/add-to-main-list', request);
        return await resp.json();
    }

    addToWatched = async (userId, item) => {
        const request = {
            method: 'POST',
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({userId, item})
        }

        const resp = await fetch('/api/lists/add-to-watched', request);

        return await resp.json();

        
    }

    getWatched = async (owner) => {
        const request = {
            method: 'POST',
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({owner})
        }

        const resp = await fetch('/api/lists/get-all-watched', request);

        return await resp.json();
    }

    reorderMainList = async (owner, list) => {
        const data = {
            owner,
            list
        }

        const request = {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }

        const resp = await fetch('/api/lists/reorder-main-list', request);
        
        return await resp.json();
    }
}