export default class TheMovieDB {
    _baseUrl = 'https://api.themoviedb.org/3';
    _api = '78e26901b9461160e9d5413e4db00ad2';
    _appendToJson = 'credits%2Csimilar%2Crecommendations';

    getPerson = async (id) => {
        const resp = await fetch(`${this._baseUrl}/person/${id}?api_key=${this._api}&language=ru-RU`);

        return await resp.json();
    }
    
    getContent = async (id, type) => {
        const resp = await fetch(`${this._baseUrl}/${type}/${id}?api_key=${this._api}&language=ru-RU&append_to_response=${this._appendToJson}`);

        return await resp.json();
    }

    getContent1 = async (id, type) => {
        const resp = await fetch(`${this._baseUrl}/${type}/${id}?api_key=${this._api}&language=ru-RU`);

        return await resp.json();
    }

    getCredits = async (id, type) => {
        const resp = await fetch(`${this._baseUrl}/${type}/${id}/credits?api_key=${this._api}&language=ru-RU`);

        return await resp.json();
    }

    getSimilar = async (id, type) => {
        const resp = await fetch(`${this._baseUrl}/${type}/${id}/similar?api_key=${this._api}&language=ru-RU&page=1`);

        return await resp.json();
    }

    getRecomended = async (id, type) => {
        const resp = await fetch(`${this._baseUrl}/${type}/${id}/recommendations?api_key=${this._api}&language=ru-RU&page=1`);

        return await resp.json();
    }

    getVideos = async (id, type, lang) => {
        const resp = await fetch(`${this._baseUrl}/${type}/${id}/videos?api_key=${this._api}&language=${lang}`);

        return await resp.json();
    }

    multiSearch = async (title) => {
        const resp = await fetch(`${this._baseUrl}/search/multi?api_key=${this._api}&language=ru-RU&query=${title}&page=1&include_adult=false&region=russia`);

        return await resp.json();
    }
}