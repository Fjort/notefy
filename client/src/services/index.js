import TheMovieDB from './tmdb-service';
import NotefyDB from './ndb-service';

export {
    TheMovieDB,
    NotefyDB
}