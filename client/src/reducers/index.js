let initialState = {
    popup: {
        state: false,
        type: null,
        subtype: null
    },
    user: {
        isLoggedIn: false,
        email: null,
        userId: null,
        login: null
    },
    mainList: [],
    listOfWatched: [],
    lastAddedItem: null
}

let reducer = (state = initialState, action) => {
    switch (action.type) {

        case "SIGN_IN":
            return {
                ...state,
                user: action.payload
            }

        case "SET_MAIN_LIST":
            const parsedList = action.payload;

            return {
                ...state,
                mainList: parsedList
            }

        case "OPEN_POPUP":
            return {
                ...state,
                popup: action.payload
            }

        case "CLOSE_POPUP":
            return {
                ...state,
                popup: action.payload
            }

        case "SET_LAST_ADDED_ITEM":
            return {
                ...state,
                lastAddedItem: action.payload
            }

        case "ADD_ITEM_TO_LIST":
            const addedItem = {...action.payload};
            if(addedItem.description.length > 135) {
                addedItem.description = addedItem.description.slice(0, 150);
            }

            return {
                ...state,
                mainList: [addedItem, ...state.mainList]
            }

        case "CHANGE_ALL_LIST":
            return {
                ...state,
                mainList: action.payload
            }

        case "SET_WATCHED_LIST":
            return {
                ...state,
                listOfWatched: action.payload
            }

        case "ADD_TO_WATCHED":
            const idOfAddToWatchedItem = action.payload.id;
            const idxOfAddToWatchedItem = state.mainList.findIndex(item => item.id === idOfAddToWatchedItem);

            return {
                ...state,
                mainList: [...state.mainList.slice(0, idxOfAddToWatchedItem), ...state.mainList.slice(idxOfAddToWatchedItem + 1)],
                listOfWatched: [action.payload, ...state.listOfWatched]
            }

        case "REMOVE_ITEM_FROM_LIST":
            const idToRemoveItemFromMainList = action.payload;
            const idxToRemoveItemFromMainList = state.mainList.findIndex(item => item.id === idToRemoveItemFromMainList);

            return {
                ...state,
                mainList: [...state.mainList.slice(0, idxToRemoveItemFromMainList), ...state.mainList.slice(idxToRemoveItemFromMainList + 1)]
            }
            
        default:
            return state;
    }
}

export default reducer;