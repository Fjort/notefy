import React from 'react';
import cn from 'classnames';

import './style.css';

export default function Button ({action, filled, title, type, isWhiteText}) {
    const btnClass = cn({
        'b-button': true,
        'b-button---filled': filled,
        'b-button---light-color': isWhiteText
    });

    return <button type={type} className={btnClass} onClick={action}>{title}</button>
}