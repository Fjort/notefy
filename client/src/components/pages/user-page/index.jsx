import React, { Component } from 'react';
import { connect } from 'react-redux';

import './style.css'

class UserPage extends Component {
    checkFields = () => {
        const inputs = document.querySelectorAll('.b-user-page--collection--form--text');

        inputs.forEach(input => {
            if(input.value.length > 0) {
                input.classList.add('b-user-page--collection--form--text---filled')
            } else {
                input.classList.remove('b-user-page--collection--form--text---filled')
            }
        })
    }

    componentDidMount() {
        this.checkFields();
    }

    render() {
        return(
            <div className="l-content--section">
                <div className="l-content--section--inside">
                    <div className="b-user-page">
                        <h1 className="b-user-page--title">Редактирование профиля</h1>
                        <div className="b-user-page--collection">
                            {/* <div className="b-user-page--collection--picture">
                                <img src="/assets/i/user-profile-photo-placeholder.svg" alt="" className="b-user-page--collection--picture--entity"/>
                            </div> */}
                            <form className="b-user-page--collection--form">
                                <div className="b-user-page--collection--form--field">
                                    <input type="text" className="b-user-page--collection--form--text" onInput={this.checkFields}/>
                                    <span className="b-user-page--collection--form--placeholder">Имя</span>
                                </div>
                                <div className="b-user-page--collection--form--field">
                                    <input type="text" className="b-user-page--collection--form--text" onInput={this.checkFields}/>
                                    <span className="b-user-page--collection--form--placeholder">Фамилия</span>
                                </div>
                                <div className="b-user-page--collection--form--field">
                                    <input type="text" defaultValue={this.props.user.login} className="b-user-page--collection--form--text" onInput={this.checkFields}/>
                                    <span className="b-user-page--collection--form--placeholder">Логин</span>
                                </div>
                                <div className="b-user-page--collection--form--field">
                                    <input type="text" defaultValue={this.props.user.email} className="b-user-page--collection--form--text" onInput={this.checkFields}/>
                                    <span className="b-user-page--collection--form--placeholder">Email</span>
                                </div>
                                {/* <div className="b-user-page--collection--form--field">
                                    <input type="text" className="b-user-page--collection--form--text" onInput={this.checkFields}/>
                                    <span className="b-user-page--collection--form--placeholder">Старый пароль</span>
                                </div>
                                <div className="b-user-page--collection--form--field">
                                    <input type="text" className="b-user-page--collection--form--text" onInput={this.checkFields}/>
                                    <span className="b-user-page--collection--form--placeholder">Новый пароль</span>
                                </div> */}
                                <div className="b-user-page--collection--form--field b-user-page--collection--form--field---submit">
                                    <button className="b-user-page--collection--form--submit" type="submit">Сохранить</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        user: state.user
    }
}

export default connect(mapStateToProps)(UserPage);