import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { addItemToList, openPopUp } from '../../../actions';
import { connect } from 'react-redux';
import { withTmDBService } from '../../../hoc';

import './style.css';

class SearchPage extends Component {
    state = {
        list: null,
        genres: [
            {
              id: 28,
              name: "боевик"
            },
            {
              id: 12,
              name: "приключения"
            },
            {
              id: 16,
              name: "мультфильм"
            },
            {
              id: 35,
              name: "комедия"
            },
            {
              id: 80,
              name: "криминал"
            },
            {
              id: 99,
              name: "документальный"
            },
            {
              id: 18,
              name: "драма"
            },
            {
              id: 10751,
              name: "семейный"
            },
            {
              id: 14,
              name: "фэнтези"
            },
            {
              id: 36,
              name: "история"
            },
            {
              id: 27,
              name: "ужасы"
            },
            {
              id: 10402,
              name: "музыка"
            },
            {
              id: 9648,
              name: "детектив"
            },
            {
              id: 10749,
              name: "мелодрама"
            },
            {
              id: 878,
              name: "фантастика"
            },
            {
              id: 10770,
              name: "телевизионный фильм"
            },
            {
              id: 53,
              name: "триллер"
            },
            {
              id: 10752,
              name: "военный"
            },
            {
              id: 37,
              name: "вестерн"
            }
          ]
    }

    getCollection = (movieName) => {
        const { tmdbService } = this.props;

        tmdbService.multiSearch(movieName)
            .then(data => {
                data.results.sort(function (a, b) {
                    if (a.popularity > b.popularity) {
                    return 1;
                    }
                    if (a.popularity < b.popularity) {
                    return -1;
                    }

                    return 0;
                }).reverse();

                this.setState({
                    list: data.results
                });
            })
            .catch(e => console.log(e));
    }

    renderItems = () => {
        const listOfMainListIds = this.props.list.map(item => item.id);

        if(this.state.list) {
            if(this.state.list.length < 1) {
                return 'ничего не найдено';
            }
            
            return this.state.list.map(({title, name, id, poster_path, overview, release_date, media_type, first_air_date, genre_ids}, i) => {
                let type = '';
                // let subtitleType = '';
                const releaseDate = release_date || first_air_date;
                const releaseStr = releaseDate ? releaseDate.split('-')[0] : '';
                let genresArr = [];
                
                if(genre_ids) {

                    genresArr = genre_ids.map(genreItem => {
                        let genre = '';
                        this.state.genres.forEach(item => {
                            if(genreItem === item.id) {
                                genre = item.name;
                            }
                        });
    
                        return genre;
                    });
                }
                const genresStr = genresArr[0] === "" ? '' : ` • ${genresArr.join(', ')}`;

                switch (media_type) {
                    case 'movie':
                        type = 'movie';
                        // subtitleType = 'фильм';
                        break;
                    case 'tv':
                        type = 'series';
                        // subtitleType = 'сериал';
                        break;
                    case 'person':
                        type = 'person';
                        break;
                
                    default:
                        break;
                }

                const formatItem = {
                    id,
                    path: `/${type}/${id}`,
                    title: title || name,
                    poster: poster_path,
                    release: releaseStr,
                    description: overview,
                    genres: genresStr
                }

                const isIn = listOfMainListIds.some(item => item === id);
                const poster = poster_path ? `https://image.tmdb.org/t/p/w185${poster_path}` : '/assets/i/list-item-image-placeholder.svg'

                return (
                    <Link to={`/${type}/${id}`} className="b-list--collection--clause" key={i} onClick={(e) => this.addItemToList(e, formatItem)}>
                        <div className="b-list--collection--clause--poster">
                            <img src={poster} alt="" className="b-list--collection--clause--poster--entity"/>
                        </div>
                        <div className="b-list--collection--clause--info">
                            <h3 className="b-list--collection--clause--info--title">{title || name}</h3>
                            <span className="b-list--collection--clause--info--genre">{releaseStr}{genresStr}</span>
                            <div className="b-list--collection--clause--info--description">
                                <p>{overview}</p>
                            </div>
                        </div>
                        <div className="b-list--collection--clause--actions">
                            <button className="b-list--collection--clause--actions--add" disabled={isIn}></button>
                            <button className="b-list--collection--clause--actions--share">
                                <img src="/assets/i/share-icon.svg" alt=""/>
                            </button>
                        </div>
                    </Link>
                )
            });
        } else {
            return '';
        }
    }

    componentDidMount() {
        const { match } = this.props;
        const { title } = match.params;

        this.getCollection(title);
    }

    componentDidUpdate(prevProps, prevState) {
        if(prevProps.match.params.title !== this.props.match.params.title) {
            this.getCollection(this.props.match.params.title);
        }
    }

    addItemToList = (e, item) => {
        if(e.target.classList.contains('b-list--collection--clause--actions--add')) {
            e.preventDefault();

            if(!this.props.user.isLoggedIn) {
                this.props.onOpenPopup({
                    state: true,
                    type: 'authorization',
                    subtype: 'login'
                });
                return;
            }

            this.props.onAddItem(item);

        }
    }

    onSubmit = (e) => {
        e.preventDefault();

        const form = e.target;
        const formData = new FormData(form);
        const value = formData.get('search');

        this.getCollection(value);
    }
    
    render() {

        return (
            <section className="l-content--section">
                <div className="l-content--section--inside">
                    <div className="b-search">
                        <div className="b-search--result">
                            <div className="b-search--result--collection">
                                {this.renderItems()}
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}

const mapStateToProps = state => {
    return {
        list: state.mainList,
        user: state.user
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onAddItem: item => dispatch(addItemToList(item)),
        onOpenPopup: popup => dispatch(openPopUp(popup))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withTmDBService()(SearchPage));