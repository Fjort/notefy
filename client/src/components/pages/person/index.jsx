import React, { Component } from 'react'
import { withTmDBService } from '../../../hoc';

import { PersonPageView } from './view';

class PersonPage extends Component {
    state = {
        info: {},
        credits: {}
    }

    componentDidMount() {
        const { match } = this.props;
        const { id } = match.params;

        this.getPersonInfo(id);
    }

    getPersonInfo = id => {
        const { tmdbService } = this.props;
        
        tmdbService.getPerson(id)
            .then(data => {
                this.setState({info: data});
            })
            .catch(e => console.log(e))

        fetch(`https://api.themoviedb.org/3/person/${id}/combined_credits?api_key=78e26901b9461160e9d5413e4db00ad2&language=ru-RU`)
            .then(body => body.json())
            .then(data =>this.setState({credits: data}))
    }

    render() {
        return <PersonPageView info={this.state.info} credits={this.state.credits}/>;
    }
}

export default withTmDBService()(PersonPage)