import React from 'react';
import { Link } from 'react-router-dom';

import './style.css';

export const PersonPageView = ({info, credits}) => {
    const {
        name,
        profile_path,
        birthday
    } = info

    function renderCredits () {
        if(credits.cast) {

            const movies = credits.cast.filter(item => item.poster_path)

            const sortedMovies = movies.sort(function (a, b) {

                const aData = parseInt(a.popularity);
                const bData = parseInt(b.popularity);
    
                if (aData > bData) {
                    return -1;
                }
                if (aData < bData) {
                    return 1;
                }
    

                return 0;
            });

            return sortedMovies.map((item, i) => {

                const title = item.title || item.name;
                const originaltitle = item.original_title || item.original_name;
                const release = item.first_air_date || item.release_date;
    
                return <Link className="b-credits--clause" to={`/${item.media_type === 'movie' ? 'movie' : 'series'}/${item.id}`} key={i}>
                    <div className="b-credits--clause--picture">
                        <img className="b-credits--clause--picture--entity" src={`https://image.tmdb.org/t/p/w92${item.poster_path}`} alt=""/>
                    </div>
                    <div className="b-credits--clause--info">
                        <h4 className="b-credits--clause--title">{title} ({release.split('-')[0]})</h4>
                        <span className="b-credits--clause--original-title">{originaltitle}</span>
                        <span className="b-credits--clause--character">{item.character ? `В роли: ${item.character}` : ''}</span>
                    </div>
                </Link>
            })
        }
    }

    const picturePlaceholderPath = '/assets/i/person-img-placeholder.svg';
    const picture = profile_path ? `https://image.tmdb.org/t/p/w500${profile_path}` : picturePlaceholderPath;

    const formatedBirthday = birthday ? new Date(birthday).toLocaleDateString() : '';

    if(info.name) {
        return (
            <section className="l-content--section l-content--section">
                <div className="l-content--section--inside">
                    <div className="b-person">
                        <div className="b-person--collection">
                            <div className="b-person--collection--clause">
                                <div className="b-person--poster">
                                    <img src={picture} alt="" className="b-person--poster--entity"/>
                                </div>
                                <h1 className="b-person--title">{name}</h1>
                                <dl className="b-person--info-clause">
                                    <dt className="b-person--info-clause--title">День рождения:</dt>
                                    <dd className="b-person--info-clause--value">{formatedBirthday}</dd>
                                </dl>
                            </div>
                            <div className="b-person--collection--clause">
                                <h2 className="b-credits--title">Актёр</h2>
                                <div className="b-credits">{renderCredits()}</div>
                            </div>
                        </div>
                    </div>
                    <img src="/assets/i/arrow-down-white.svg" alt="" className="b-person-arrow-down"/>
                </div>
            </section>
        );
    } else {
        return null
    }
}