import React from 'react';
import { connect } from 'react-redux';

import { openPopUp } from '../../../actions';

const Trailers = ({trailers, onPlayTrailer, createRef}) => {

    if(trailers.length > 0) {
        const trailersCollection = trailers.map(({id, key}) => {
                const trailerObj = {
                    state: true,
                    type: 'trailer',
                    subType: key
                }

                return <div className="b-trailers--colletion--clause" key={id} onClick={() => onPlayTrailer(trailerObj)}>
                    <div className="b-trailers--colletion--clause--picture">
                        <img src={`https://img.youtube.com/vi/${key}/hqdefault.jpg`} alt="" className="b-trailers--colletion--clause--picture--entity"/>
                        <img src="/assets/i/play-icon.svg" className="b-trailers--colletion--clause--picture--play" alt=""/>
                    </div>
                </div>
            }
        );
    
        return (
            <section className="l-content--section">
                <div className="l-content--section--inside">
                    <div className="b-cards b-cards---trailers">
                        <h2 className="b-cards--title">Трейлеры</h2>
                        <div className="b-trailers--collection">
                            {trailersCollection}
                        </div>
                    </div>
                </div>
            </section>
        );
    } else {
        return null;
    }

}

const mapStateToProps = state => {
    return {}
}

const mapDispatchToProps = dispatch => {
    return {
        onPlayTrailer: trailer => dispatch(openPopUp(trailer))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Trailers);