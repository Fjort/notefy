import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { addItemToList, removeItemFromList, openPopUp } from '../../../actions';
import { animateScroll as scroll } from 'react-scroll';
import { withNDBService } from '../../../hoc';

import CardsCollection from '../../cards-collection';
import Button from '../../button';
import Trailers from './trailers';

import './style.css';

const ContentPageView = ({ onOpenPopup, mainInfo, cast, similar, recommended, trailers, onAddItemToList, mainList, onRemoveItemToList, user, ndbService, lastAddedItem }) => {
    const {
        id,
        poster,
        background,
        title,
        overview,
        release,
        genres
    } = mainInfo;

    const backDrop = background ? `https://image.tmdb.org/t/p/w1280${background}` : '../assets/i/backdrop-default2.jpg';

    const formatedItem = {
        id,
        path: window.location.pathname,
        title: title,
        poster: poster,
        release: release,
        description: overview,
        genres: ` • ${genres}`
    }

    function checkLogAndDoAction () {
        if(!user.isLoggedIn) {
            onOpenPopup({
                state: true,
                type: 'authorization',
                subtype: 'login'
            });
            return;
        }

        onAddItemToList(formatedItem)
    }

    function deleteItemFromMainList () {
        let deletedItemId = null;

        if(lastAddedItem) {
            deletedItemId = lastAddedItem;
        } else {            
            const itemToDelele = mainList.find(item => item.id === id);
            deletedItemId = itemToDelele._id;
        }

        ndbService.deleteFromMainList(user.userId, deletedItemId);
        onRemoveItemToList(id);
    }

    const isIn = mainList.some(item => item.id === id);

    let actionButton = isIn ? <Button type={"button"} action={deleteItemFromMainList} title={"Удалить из списка"} filled={true}/> : <Button type={"button"} action={checkLogAndDoAction} title={"Добавить"} filled={true}/>;

    const type = window.location.pathname.split('/')[1];

    return(
        <Fragment>
            <section className="l-content--section l-content--section---movie" style={{'backgroundImage': `url(${backDrop})`}}>
                <div className="l-content--section--inside">
                    <div className="b-content-main">
                        <div className="b-content-main--collection">
                            <div className="b-content-main--collection--clause">
                                <div className="b-content-main--poster">
                                    <img src={`https://image.tmdb.org/t/p/w500${poster}`} alt="" className="b-content-main--poster--entity"/>
                                </div>
                                <div className="b-content-main--actions">
                                    <div className="b-content-main--actions--clause">
                                        {actionButton}
                                    </div>
                                    <div className="b-content-main--actions--clause">
                                        <Button type={"button"} action={scroll.scrollToBottom} title={"Трейлеры"} filled={false} isWhiteText={true}/>
                                    </div>
                                </div>
                            </div>
                            <div className="b-content-main--collection--clause">
                                <MainInfo info={mainInfo}/>
                            </div>
                        </div>
                    </div>
                    <img src="/assets/i/arrow-down-white.svg" alt="" className="b-content-main-arrow-down"/>
                </div>
            </section>
            {cast.length > 0 ? <CardsCollection title={"В главных ролях"} collection={cast} mediaType={'person'}/> : null}
            {similar.length > 0 ? <CardsCollection title={"Похожие фильмы"} collection={similar} mediaType={type}/> : null}
            {recommended.length > 0 ? <CardsCollection title={"Должно понравиться"} collection={recommended} mediaType={type}/> : null}
            <Trailers trailers={trailers}/>
        </Fragment>
    );
}

const MainInfo = ({info}) => {
    const type = window.location.pathname.split('/')[1];
    const {
        title,
        originalTitle,
        seasons,
        overview,
        runtime,
        release,
        createdBy,
        genres,
        directors
    } = info;

    const isCreatedBy = () => {
        if(createdBy) {
            return <span className="b-content-main--line">Создатель • {createdBy}</span>;
        } else {
            return null;
        }
    }

    if(type === 'series') {
        return (
            <Fragment>
                <h1 className="b-content-main--title">{title}</h1>
                <span className="b-content-main--original-title">{originalTitle}</span>
                <span className="b-content-main--date-genre">{release} • {genres}</span>
                {isCreatedBy()}
                <span className="b-content-main--line">Сезонов • {seasons}</span>
                <span className="b-content-main--line">Продолжительность серии • {runtime} мин.</span>
                <h3 className="b-content-main--description-title">Описание</h3>
                <p className="b-content-main--description">{overview}</p>
            </Fragment>
        );
    } else if (type === 'movie') {
        return (
            <Fragment>
                <h1 className="b-content-main--title">{title}</h1>
                <span className="b-content-main--original-title">{originalTitle}</span>
                <span className="b-content-main--date-genre">{release} • {genres}</span>
                <span className="b-content-main--line">Режиссер • {directors}</span>
                <span className="b-content-main--line">Продолжительность • {runtime} мин.</span>
                <h3 className="b-content-main--description-title">Описание</h3>
                <p className="b-content-main--description">{overview}</p>
            </Fragment>
        );
    } else {
        return null;
    }

}

const mapStateToProps = state => {
    return {
        mainList: state.mainList,
        user: state.user,
        lastAddedItem: state.lastAddedItem
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onAddItemToList: item => dispatch(addItemToList(item)),
        onRemoveItemToList: id => dispatch(removeItemFromList(id)),
        onOpenPopup: popup => dispatch(openPopUp(popup))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withNDBService()(ContentPageView));