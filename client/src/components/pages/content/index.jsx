import React, { Component } from 'react';
import { withTmDBService  } from '../../../hoc';
import { animateScroll as scroll } from 'react-scroll';

import ContentPageView from './view';

class ContentPage extends Component {
    state = {
        details: null,
        isLoaded: false,
        trailers: []
    }

    componentDidMount() {
        const { match } = this.props;
        const { id } = match.params;
        const type = this.getMediaType();

        this.getContent(id);
        this.getTrailers(id, type, 'ru-RU');
    }

    componentDidUpdate(prevProps, prevState) {
        if(prevProps.match.params.id !== this.props.match.params.id) {

            const { match } = this.props;
            const { id } = match.params;
            const type = this.getMediaType();

            this.getContent(id);
            this.getTrailers(id, type, 'ru-RU');

            scroll.scrollToTop();
        }
    }

    getTrailers = (id, type, lang) => {
        const { tmdbService } = this.props;

        tmdbService.getVideos(id, type, lang)
            .then(data => {
                if(data.results.length > 0) {
                    const newData = data.results.filter((item, i) => i < 3);
                    this.setState({
                        trailers: newData
                    });
                } else {
                    tmdbService.getVideos(id, type, 'en-US')
                        .then(data => {
                            if(data.results.length > 0) {
                                const newData = data.results.filter((item, i) => i < 3);
                                this.setState({
                                    trailers: newData
                                });
                            } else {
                                return;
                            }
                        })
                }
            })
    }

    getContent = (id) => {
        const { tmdbService } = this.props;
        const path = window.location.pathname.split('/')[1];
        let type = '';
        
        if(path === 'series') {
            type = 'tv';
        } else {
            type = 'movie';
        }
        
        tmdbService.getContent(id, type)
            .then(data => {

                this.setState({
                    details: data,
                    isLoaded: true
                });
            });
    }

    formatDetails = details => {
        let release = '';
        let createdBy = '';

        if(details.first_air_date) {
            release = details.first_air_date.split('-')[0];
        }

        if(details.release_date) {
            release = details.release_date.split('-')[0];
        }

        if(details.created_by) {
            createdBy = details.created_by
                .map(item => {
                    return item.name;
                })
                .join(', ');
        }

        const cast = details.credits.cast.slice(0, 6);
        const correctCast = cast.map(item => {
            return {
                id: item.id,
                picture: item.profile_path,
                title: item.name,
                subTitle: item.character
            }
            
        });

        const similarSorted = details.similar.results.sort(function (a, b) {
            if (a.popularity > b.popularity) {
            return 1;
            }
            if (a.popularity < b.popularity) {
            return -1;
            }

            return 0;
        }).reverse();
        const similar = similarSorted.slice(0, 6);
        const correctSimilar = similar.map(item => {
            const releaseDate = item.release_date || item.first_air_date;
            const releaseYear = releaseDate ? releaseDate.split('-')[0] : '';

            return {
                id: item.id,
                picture: item.poster_path,
                title: item.title || item.name,
                subTitle: releaseYear
            }
            
        });

        const recommendedSorted = details.recommendations.results.sort(function (a, b) {
            if (a.popularity > b.popularity) {
                return 1;
            }
            if (a.popularity < b.popularity) {
                return -1;
            }

            return 0;
        }).reverse();
        const recommended = recommendedSorted.slice(0, 6);
        const correctRecommended = recommended.map(item => {
            const releaseDate = item.release_date || item.first_air_date;
            const releaseYear = releaseDate.split('-')[0];

            return {
                id: item.id,
                picture: item.poster_path,
                title: item.title || item.name,
                subTitle: releaseYear
            }
            
        });

        const genres = details.genres.map(item => {
            return item.name;
        }).join(', ');

        const directors = details.credits.crew
            .filter(item => item.job === 'Director')
            .map(item => {
                return item.name;
            })
            .join(', ');

        const fullDetails = {
            mainInfo: {
                id: details.id,
                poster: details.poster_path,
                background: details.backdrop_path,
                title: details.title || details.name,
                rating: details.vote_average,
                originalTitle: details.original_title || details.original_name,
                seasons: details.number_of_seasons ? details.number_of_seasons : false,
                overview: details.overview,
                runtime: details.runtime || (details.episode_run_time ? details.episode_run_time[0] : ''),
                release,
                createdBy,
                genres,
                directors
            },
            cast: correctCast,
            similar: correctSimilar,
            recommended: correctRecommended
        }

        return fullDetails;
    }

    getMediaType = () => {
        const { match } = this.props;
        const splitedType = match.path.split('/')[1];
        
        if(splitedType === 'series') {
            return 'tv';
        } else {
            return 'movie';
        }
    }


    render() {
        const { details, isLoaded, trailers } = this.state;

        if(isLoaded) {
            const fullDetails = this.formatDetails(details);
            const { mainInfo, cast, similar, recommended } = fullDetails;

            return <ContentPageView 
                mainInfo={mainInfo}
                cast={cast}
                similar={similar}
                recommended={recommended}
                trailers={trailers}
            />;
        } else {
            return null;
        }
    }
}

export default withTmDBService()(ContentPage)