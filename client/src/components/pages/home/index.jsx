import React from 'react';
import { connect } from 'react-redux';
import { openPopUp } from '../../../actions';
import Button from '../../button';

import './style.css';

const HomePage = (props) => {
    const authorizationPopup = {
        login: {
            state: true,
            type: 'authorization',
            subtype: 'login'
        },
        registration: {
            state: true,
            type: 'authorization',
            subtype: 'registration'
        }
    }

    return (
        <React.Fragment>
            <section className="l-content--section">
                <div className="l-content--section--inside">
                    <div className="b-promo">
                        <div className="b-promo--picture">
                            <img src="/assets/i/main-promo-picture.jpg" alt="" className="b-promo--picture--entity"/>
                        </div>
                        <div className="b-promo--info">
                            <h1 className="b-promo--title">Привет! Мы&nbsp;создали этот проект, чтобы сделать поиск фильмов на&nbsp;вечер еще удобнее и&nbsp;быстрее.</h1>
                            <div className="b-promo--description">
                                <p>Здесь ты&nbsp;сможешь создать свой список фильмов, которые хочешь посмотреть, узнать какие фильмы идут в&nbsp;кино и&nbsp;получить свою персональную подборку на&nbsp;основе того, что ты&nbsp;добавил в&nbsp;свой список.</p>
                                <i>Приятного пользования!</i>
                            </div>
                        </div>
                        <div className="b-promo--actions">
                            <div className="b-promo--actions--clause">
                                <Button type={"button"} action={() => props.onOpenPopup(authorizationPopup.registration)} title={"Зарегистрироваться"} filled={true}/>
                            </div>
                            <div className="b-promo--actions--clause">
                                <Button type={"button"} action={() => props.onOpenPopup(authorizationPopup.login)} title={"Войти"} filled={false}/>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section className="l-content--section">
                <div className="l-content--section--inside">
                    <div className="b-about">
                        <div className="b-about--collection">
                            <div className="b-about--collection--row">
                                <div className="b-about--collection--row--clause b-about--collection--row--clause---picture">
                                    <div className="b-about--collection--row--clause--picture">
                                        <img src="/assets/i/about-1.jpg" alt="" className="b-about--collection--row--clause--picture--entity"/>
                                    </div>
                                </div>
                                <div className="b-about--collection--row--clause b-about--collection--row--clause---info">
                                    <h3 className="b-about--collection--row--clause--title">Смотри по&nbsp;списку</h3>
                                    <div className="b-about--collection--row--clause--description">
                                        <p>Создай свой персональный список фильмов. Отмечай фильмы, которые хочешь посмотреть и&nbsp;какие уже посмотрел.</p>
                                    </div>
                                </div>
                            </div>
                            <div className="b-about--collection--row">
                                <div className="b-about--collection--row--clause b-about--collection--row--clause---info">
                                    <h3 className="b-about--collection--row--clause--title">Ищи по&nbsp;настроению</h3>
                                    <div className="b-about--collection--row--clause--description">
                                        <p>Посмотри наши подборки, которые мы&nbsp;составили для тебя. Найди подходящую по&nbsp;настроению подборку и&nbsp;выбирай</p>
                                        <p>Или составь свою собственную!</p>
                                    </div>
                                </div>
                                <div className="b-about--collection--row--clause b-about--collection--row--clause---picture">
                                    <div className="b-about--collection--row--clause--picture">
                                        <img src="/assets/i/about-2.jpg" alt="" className="b-about--collection--row--clause--picture--entity"/>
                                    </div>
                                </div>
                            </div>
                            <div className="b-about--collection--row">
                                <div className="b-about--collection--row--clause b-about--collection--row--clause---picture">
                                    <div className="b-about--collection--row--clause--picture">
                                        <img src="/assets/i/about-3.jpg" alt="" className="b-about--collection--row--clause--picture--entity"/>
                                    </div>
                                </div>
                                <div className="b-about--collection--row--clause b-about--collection--row--clause---info">
                                    <h3 className="b-about--collection--row--clause--title">Будь в&nbsp;теме</h3>
                                    <div className="b-about--collection--row--clause--description">
                                        <p>Узнавай о&nbsp;всех вышедших фильмах, сериалах и&nbsp;трейлерах первым!</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </React.Fragment>
    );
}

const mapStateToProps = state => {
    return {

    }
}

const mapDispatchToProps = dispatch => {
    return {
        onOpenPopup: popup => dispatch(openPopUp(popup))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);