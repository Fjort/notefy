import React, { Component, Fragment } from 'react';
import { Link } from 'react-router-dom';
import ListCollection from '../../list-collection';
import { changeAllList, addToWatched } from '../../../actions';
import { connect } from 'react-redux';
import { withNDBService } from '../../../hoc';

import './style.css';
import { DragDropContext } from 'react-beautiful-dnd';

class RegistredHome extends Component {
    state = {
        list: []
    }

    componentDidMount() {
        this.setState({
            list: this.props.list
        });
    }

    componentDidUpdate(prevProps, prevState) {
        if(prevProps.list !== this.props.list) {

            this.setState({
                list: this.props.list
            });
        }
    }

    onDragEnd = result => {
        const { destination, source } = result;
        
        if(!destination) {
            return;
        }

        const newList = [...this.state.list];
        const sourceItem = newList[source.index];

        newList.splice(source.index, 1);
        newList.splice(destination.index, 0, sourceItem);

        const listIds = newList.map(item => item._id);
        const isNull = listIds.some(item => item === null);

        if(!isNull) {
            this.props.ndbService.reorderMainList(this.props.userId, listIds);
        }

        this.setState({list: newList});
        this.props.onChangeList(newList);
    }

    deleteItem = (idx) => {
        const oldList = [...this.state.list];
        const newList = oldList.filter((item, i) => i !== idx);
        const deletedId = oldList[idx]._id;

        this.props.ndbService.deleteFromMainList(this.props.userId, deletedId);

        this.setState({list: newList});
        this.props.onChangeList(newList);
    }

    markAsWatched = item => {
        this.props.onAddToWatched(item);

        this.props.ndbService.addToWatched(this.props.userId, item);
    }

    render() {

        return (
            <Fragment>
                <section className="l-content--section">
                    <div className="l-content--section--inside">
                        <div className="b-list---home">
                            <h1 className="b-list--title">Мой список</h1>
                            <DragDropContext onDragEnd={this.onDragEnd}>
                                <ListCollection list={this.state.list} deleteItem={this.deleteItem} markAsWatched={this.markAsWatched}/>
                            </DragDropContext>
                            {this.props.list.length > 0 ? <Link to={`/list/${this.props.user.login}`} className="b-list--open-all" onClick={() => window.scrollTo(0,0)}>Перейти в список</Link> : null}
                        </div>
                    </div>
                </section>
                <section className="l-content--section">
                    <div className="l-content--section--inside">
                        <div className="b-cards">
                            <h1 className="b-cards--title">Новинки</h1>
                            
                        </div>
                    </div>
                </section>
            </Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        list: state.mainList,
        userId: state.user.userId,
        user: state.user
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onChangeList: newList => dispatch(changeAllList(newList)),
        onAddToWatched: item => dispatch(addToWatched(item))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withNDBService()(RegistredHome));