import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import './style.css';

class WatchedPage extends Component {
    state = {
        list: []
    }

    renderItems = () => {
        return this.state.list.map(({title, poster, description, release, id, path, genres}) => {

                const posterPath = poster ? `https://image.tmdb.org/t/p/w185${poster}` : '/assets/i/list-item-image-placeholder.svg'

                return (
                    <Link to={path} className="b-list--collection--clause" key={id}>
                        <div className="b-list--collection--clause--poster">
                            <img src={posterPath} alt="" className="b-list--collection--clause--poster--entity"/>
                        </div>
                        <div className="b-list--collection--clause--info">
                            <h3 className="b-list--collection--clause--info--title">{title}</h3>
                            <span className="b-list--collection--clause--info--genre">{release}{genres}</span>
                            <div className="b-list--collection--clause--info--description">
                                <p>{description}</p>
                            </div>
                        </div>
                    </Link>
                )
            });
    }

    componentDidMount() {
        this.setList();
    }

    componentDidUpdate(prevProps, prevState) {
        if(prevProps.list !== this.props.list) {
            this.setList();
        }
    }

    setList = () => {
        const { list } = this.props;

        this.setState({list});
    }
    
    render() {
        const { list } = this.state;

        if(list.length > 0) {

            return (
                <section className="l-content--section">
                    <div className="l-content--section--inside">
                        <div className="b-watched">
                            <h1 className="b-watched--title">Список просмотренного</h1>
                            <div className="b-watched--result--collection">
                                {this.renderItems()}
                            </div>
                        </div>
                    </div>
                </section>
            )
        } else {
            return (
                <section className="l-content--section">
                    <div className="l-content--section--inside">
                        <div className="b-watched">
                            <h1 className="b-watched--title">Список просмотренного</h1>
                            <p>Список пуст</p>
                        </div>
                    </div>
                </section>
            )
        }

    }
}

const mapStateToProps = state => {
    return {
        list: state.listOfWatched
    }
}

const mapDispatchToProps = dispatch => {
    return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(WatchedPage);