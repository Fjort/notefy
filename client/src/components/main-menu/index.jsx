import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import './style.css';

function MainMenu (props) {
    function closeMainMenu () {
        const menu = document.querySelector('.l-main-menu');
        menu.classList.remove('l-main-menu---open');
    }

    function logOut () {
        localStorage.removeItem('userId');
        window.location.pathname = '/';
    }

    return (
        <div className="l-main-menu">
            <div className="b-main-menu">
                <button className="b-main-menu--close" onClick={() => closeMainMenu()}></button>
                <ul className="b-main-menu--collection">
                    <Link to={`/list/${props.login}`} className="b-main-menu--collection--link" onClick={() => closeMainMenu()}>Список</Link>
                    <Link to={`/watched/${props.login}`} className="b-main-menu--collection--link" onClick={() => closeMainMenu()}>Список просмотренного</Link>
                    <Link to={'/profile'} className="b-main-menu--collection--link" onClick={() => closeMainMenu()}>Профиль</Link>
                    <Link to={'/'} className="b-main-menu--collection--link" onClick={() => closeMainMenu()}>Что в кино?</Link>
                    <Link to={'/'} className="b-main-menu--collection--link" onClick={logOut}>Выйти</Link>
                    <Link to={'/'} className="b-main-menu--collection--link" onClick={() => closeMainMenu()}>Поддержать проект</Link>
                </ul>
            </div>
        </div>
    );
}

const mapStateToProps = state => {
    return {
        login: state.user.login
    }
}

export default connect(mapStateToProps)(MainMenu);