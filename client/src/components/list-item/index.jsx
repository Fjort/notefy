import React from 'react';
import { Draggable } from 'react-beautiful-dnd';
import { Link } from 'react-router-dom';
import { addToWatched } from '../../actions';
import { connect } from 'react-redux';

import './style.css'

function ListItem ({info, idx, deleteItem, markAsWatched}) {
    const {title, poster, description, release, id, path, genres} = info;

    function addToWatched (e) {
        if(e.target.classList.contains('b-list--collection--clause--actions--action') || e.target.classList.contains('b-list--collection--clause--actions--action--img') || e.target.classList.contains('b-list--collection--clause--actions')) {
            e.preventDefault();
        }
    }

    let posterPath = poster ? `https://image.tmdb.org/t/p/w185${poster}` : '/assets/i/list-item-image-placeholder.svg';

    return (
        <Draggable draggableId={String(id)} index={idx}>
            {(provided, snapshot) => {
                let clauseClasses = ['b-list--collection--clause'];
                if(snapshot.isDragging) {
                    clauseClasses.push('b-list--collection--clause---dragging');
                } else {
                    clauseClasses = ['b-list--collection--clause'];
                }
                
                return (
                    <Link to={path} ref={provided.innerRef} {...provided.draggableProps} {...provided.dragHandleProps} className={clauseClasses.join(' ')} onClick={(e) => addToWatched(e)}>
                        <div className="b-list--collection--clause--poster">
                            <img src={posterPath} alt="" className="b-list--collection--clause--poster--entity"/>
                        </div>
                        <div className="b-list--collection--clause--info">
                            <h3 className="b-list--collection--clause--info--title">{title}</h3>
                            <span className="b-list--collection--clause--info--genre">{release}{genres}</span>
                            <div className="b-list--collection--clause--info--description">
                                <p>{description}</p>
                            </div>
                        </div>
                        <div className="b-list--collection--clause--actions">
                            <button className="b-list--collection--clause--actions--action" onClick={() => markAsWatched(info)}>
                                <img src="/assets/i/eye.svg" alt="" className="b-list--collection--clause--actions--action--img"/>
                            </button>
                            <button className="b-list--collection--clause--actions--action" onClick={() => deleteItem(idx)}>
                                <img src="/assets/i/trash.svg" alt="" className="b-list--collection--clause--actions--action--img"/>
                            </button>
                            {/* <button className="b-list--collection--clause--actions--action">
                                <img src="/assets/i/context.svg" alt="" className="b-list--collection--clause--actions--action--img"/>
                            </button> */}
                        </div>
                    </Link>
                );
            }}
        </Draggable>
    );
}

const mapSatateToProps = state => {
    return {}
}

const mapDispatchToProps = dispatch => {
    return {
        onAddToWatched: item => dispatch(addToWatched(item))
    }
}

export default connect(mapSatateToProps, mapDispatchToProps)(ListItem);