import React from 'react';
import CardsItem from '../cards-item';
import './style.css';

export default function CardsCollection ({collection, mediaType, title}) {
    const renderCollection = () => {
        return collection.map(item => {
            return <CardsItem card={item} mediaType={mediaType} key={item.id}/>
        });
    }

    return (
        <section className="l-content--section">
            <div className="l-content--section--inside">
                <div className="b-cards">
                    <h2 className="b-cards--title">{title}</h2>
                    <div className="b-cards--collection">
                        {renderCollection()}
                    </div>
                </div>
            </div>
        </section>

    );
}