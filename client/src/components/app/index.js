import React, { Component } from 'react';
import { connect } from 'react-redux';
import { signIn, setMainList, setWatchedList, setLastAddedItem } from '../../actions';
import { withNDBService } from '../../hoc';

import Header from '../header';
import Routes from '../../routes';

import './style.css';
import Popup from '../popup';
import MainMenu from '../main-menu';

class App extends Component {

    state = {
        isPopup: false,
        isLoggedIn: false,
        quantity: 0,
        watchedQuantity: 0,
        isLoaded: false
    }
    
    componentDidMount() {
        this.checkAuthorization();
        this.checkPopup();
        this.setQuantity();
        this.setWatchedQuantity();
        const userId = localStorage.getItem('userId');
        if(!userId) {
            this.setState({ isLoaded: true });
            this.addScrollListener();
        }

    }

    componentDidUpdate(prevProps, prevState) {
        if(prevProps.popup !== this.props.popup) {
            this.checkPopup();
        }

        if(prevProps.isLoggedIn !== this.props.isLoggedIn) {
            this.loggUserIn();
        }

        if(prevProps.mainList !== this.props.mainList && prevProps.mainList.length < this.props.mainList.length) {
            if(this.props.mainList.length > 0) {
                const { userId } = this.props;
                const item = this.props.mainList[0];

                const reqObject = {userId, item};
                
                this.props.ndbService.addToMainList(reqObject)
                    .then(data => this.props.onSetLastAddedItem(data.addedItemId));
            }
        }

        if(prevProps.quantity !== this.props.quantity) {
            this.setQuantity();
        }

        if(prevProps.watchedQuantity !== this.props.watchedQuantity) {
            this.setWatchedQuantity();
        }

        if(prevState.isLoaded !== this.state.isLoaded) {
            this.addScrollListener();
        }
    }

    addScrollListener = () => {
        const header = document.querySelector('.b-header');
        window.addEventListener('scroll', e => {
            if(window.pageYOffset > 0) {
                if(header) {
                    header.classList.add('b-header---scrolled');
                }
            } else {
                if(header) {
                    header.classList.remove('b-header---scrolled');
                }
            }
        });
    }

    setQuantity = () => {
        const { quantity } = this.props;

        this.setState({quantity});
    }

    setWatchedQuantity = () => {
        const { watchedQuantity } = this.props;

        this.setState({watchedQuantity});
    }

    checkPopup = () => {
        const { popup } = this.props;

        this.setState({
            ...this.state,
            isPopup: popup.state
        });
    }

    loggUserIn = () => {
        this.setState({isLoggedIn: true});
    }
    
    checkAuthorization = () => {
        const userId = localStorage.getItem('userId');
        if(!userId) {
            return;
        }

        const { ndbService } = this.props;

        ndbService.authorize(userId)
            .then(data => {
                if(data.status === 200) {
                    this.props.onSignIn({
                        isLoggedIn: true,
                        email: data.userEmail,
                        userId: data.userId,
                        login: data.login
                    });

                    ndbService.getMainList(this.props.userId)
                        .then(data => this.props.onSetMainList(data));

                    ndbService.getWatched(data.userId)
                        .then(data => this.props.onSetWatchedList(data));
                    this.setState({ isLoggedIn: true });
                    this.setState({ isLoaded: true });
                } else {
                    localStorage.removeItem('userId');
                    this.setState({ isLoaded: true });
                }
            })
    }

    render() {
        const {
            isPopup,
            isLoggedIn,
            quantity,
            watchedQuantity,
            isLoaded
        } = this.state;

        if(isLoaded) {

            return (
                <div className="l-outer">
                    <Header isLoggedIn={isLoggedIn} quantity={quantity} watchedQuantity={watchedQuantity} login={this.props.login}/>
                    <main className="l-content">
                        <Routes isLoggedIn={isLoggedIn}/>
                    </main>
                    {/* <footer className="l-footer">
                        <Footer/>
                    </footer> */}
                    {isLoggedIn ? <MainMenu /> : null}
                    {isPopup ? <Popup /> : null}
                </div>
            );
        } else {
            return 'null'
        }

    }
}

const mapStateToProps = state => {
    return {
        isLoggedIn: state.user.isLoggedIn,
        popup: state.popup,
        quantity: state.mainList.length,
        watchedQuantity: state.listOfWatched.length,
        mainList: state.mainList,
        userId: state.user.userId,
        login: state.user.login
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onSignIn: user => dispatch(signIn(user)),
        onSetMainList: list => dispatch(setMainList(list)),
        onSetWatchedList: list => dispatch(setWatchedList(list)),
        onSetLastAddedItem: id => dispatch(setLastAddedItem(id))
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(withNDBService()(App));