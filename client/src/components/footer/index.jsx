import React from 'react';

import './style.css';

export default function Footer () {
    return(
        <section className="l-footer--section">
            <div className="l-footer--section--inside">
                <div className="b-footer">
                    <div className="b-logo">
                        <img src="/assets/i/logo.svg" alt="" className="b-logo--entity"/>
                    </div>
                </div>
            </div>
        </section>
    );
}