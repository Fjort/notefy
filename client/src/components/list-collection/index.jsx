import React, { Component } from 'react';
import ListItem from '../list-item';
import { Droppable } from 'react-beautiful-dnd';

export default class ListCollection extends Component {
    

    render() {
        if(this.props.list.length < 1) {
            return (
                <div className="b-list--collection">
                    Ваш список пока пуст
                </div>
            );
        }

        return (
                <Droppable droppableId="main">
                    {provided => {
                        return (
                            <div {...provided.droppableProps} ref={provided.innerRef} className="b-list--collection">
                                {
                                    this.props.list.map((item, i) => {
                                        return <ListItem info={item} id={item.id} key={item.id} idx={i} deleteItem={this.props.deleteItem} markAsWatched={this.props.markAsWatched}/>
                                    })
                                }
                                {provided.placeholder}
                            </div>
                        );
                    }}
                </Droppable>
        );
    }
}