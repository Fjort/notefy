import React, { Component } from 'react';
import { connect } from 'react-redux';
import { closePopUp } from '../../actions';

import AuthorizationPopup from './authorization';
import Trailer from './trailer';

import './style.css'

class Popup extends Component {
    
    renderPopup = () => {
        const { type } = this.props.popup;

        switch (type) {
            case 'authorization':
                return <AuthorizationPopup />
            case 'trailer':
                return <Trailer />
            default:
                break;
        }
    }

    popupRef = React.createRef();

    handleClickOutside = (e) => {
        if(!this.popupRef.current.contains(e.target)) {
            this.props.onClosePopUp();
        }
    }

    handleCloseByEscape = (e) => {
        if (e.code === 'Escape') {
            this.props.onClosePopUp();
        }
    }

    componentDidMount() {
        document.addEventListener('mouseup', this.handleClickOutside);
        document.addEventListener('keyup', this.handleCloseByEscape);
    }

    componentWillUnmount() {
        document.removeEventListener('mouseup', this.handleClickOutside);
        document.removeEventListener('keyup', this.handleCloseByEscape);
    }

    render() {
        return (
            <div className="l-popup">
                <div className="b-popup" ref={this.popupRef}>
                    {this.renderPopup()}
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        popup: state.popup
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onClosePopUp: () => dispatch(closePopUp())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Popup);