import React from 'react';
import Button from '../../button';
import { connect } from 'react-redux';
import { signIn, closePopUp, setMainList, setWatchedList, openPopUp } from '../../../actions';
import { withNDBService } from '../../../hoc';

import './style.css';

const AuthorizationBlock = ({type, onSignIn, onClosePopup, ndbService, onSetMainList, onSetWatchedList, onOpenPopup}) => {
    let popup = {
        img: '/assets/i/authorization-img.svg',
        title: '',
        form: null
    }

    switch (type) {
        case 'login':
            popup.img = '/assets/i/authorization-img.svg';
            popup.title = 'Вход';
            popup.form = <LogIn onOpenPopup={onOpenPopup} close={onClosePopup} action={onSignIn} ndbService={ndbService} onSetMainList={onSetMainList} onSetWatchedList={onSetWatchedList}/>
            break;
        case 'registration':
            popup.img = '/assets/i/authorization-img-reg.svg';
            popup.title = 'Регистрация';
            popup.form = <Registration close={onClosePopup} action={onSignIn} ndbService={ndbService} onSetMainList={onSetMainList} onSetWatchedList={onSetWatchedList}/>
            break;
        default:
            break;
    }

    return (
        <div className="b-authorization--collection">
            <div className="b-authorization--collection--clause">
                <div className="b-authorization--picture">
                    <img src={popup.img} alt="" className="b-authorization--picture--entity"/>
                </div>
            </div>
            <div className="b-authorization--collection--clause">
                <h3 className="b-authorization--title">{popup.title}</h3>
                {popup.form}
            </div>
        </div>
    );
}

const LogIn = ({action, close, ndbService, onSetMainList, onSetWatchedList, onOpenPopup}) => {

    const signInHandler = (e) => {
        e.preventDefault();

        const formData = new FormData(e.target);

        ndbService.signIn(formData)
            .then(data => {
                if(data.status === 400) {
                    if(data.error === "login") {
                        document.querySelector('.b-authorization--form--text---login')
                            .classList
                            .add('b-authorization--form--text---error')
                    }
                    if(data.error === "pass") {
                        document.querySelector('.b-authorization--form--text---password')
                            .classList
                            .add('b-authorization--form--text---error')
                    }
                }
                if(data.status === 200) {

                    localStorage.setItem('userId', data.userId);
                    action({
                        isLoggedIn: true,
                        email: data.userEmail,
                        userId: data.userId,
                        login: data.login
                    });
                    ndbService.getMainList(data.userId)
                        .then(data => onSetMainList(data));
    
                    ndbService.getWatched(data.userId)
                        .then(data => onSetWatchedList(data));
                    close();
                }
            });
    }

    function openRegistrationPopup () {
        close();
        onOpenPopup({
            state: true,
            type: 'authorization',
            subtype: 'registration'
        })
    }

    function clearErrorType (e) {
        e.target
            .classList
            .remove('b-authorization--form--text---error')
    }

    return (
        <React.Fragment>
            <form className="b-authorization--form" onSubmit={signInHandler}>
                <div className="b-authorization--form--field">
                    <input type="email" className="b-authorization--form--text b-authorization--form--text---login" name="email" placeholder="Логин или Email" required onInput={clearErrorType}/>
                </div>
                <div className="b-authorization--form--field">
                    <input type="password" className="b-authorization--form--text b-authorization--form--text---password" name="password" placeholder="Пароль" required onInput={clearErrorType} autoComplete="true"/>
                </div>
                <Button type={"submit"} title={"Войти"} filled={true}/>
            </form>
            <button className="b-authorization--reg" onClick={openRegistrationPopup} type="button">Зарегистрироваться</button>
        </React.Fragment>
    );
}

const Registration = ({action, close, ndbService}) => {

    let isOpenErr = false;
    let errorRef = React.createRef();

    function clearErrorType (e) {
        e.target
            .classList
            .remove('b-authorization--form--text---error')
    }

    function showErr (text) {
        isOpenErr = true;
        errorRef.current.textContent = text;
        errorRef.current
            .classList
            .add('b-authorization--error-text---visible');

        setTimeout(() => {
            if(errorRef.current && isOpenErr) {
                errorRef.current
                    .classList
                    .remove('b-authorization--error-text---visible');
                errorRef.current.textContent = '';

                isOpenErr = false;
            }
        }, 5000);
    }

    const signUpHandler = (e) => {
        e.preventDefault();

        const formData = new FormData(e.target);
        const regular = /^([A-Za-z0-9_\-\.])+\@([a-zA-ZА-Яа-я0-9_\-\.])+\.([a-zA-ZА-Яа-я]{2,5})/;
        const formObj = {};

        formData.forEach((value, key) => formObj[key] = value);

        if(regular.test(formObj.email) === false) {
            showErr('Введите корректный Email');
            document.querySelector('.b-authorization--form--text---reg-email')
                .classList
                .add('b-authorization--form--text---error');

            return;
        }
        if(formObj.password.length < 6) {
            showErr('Пароль должен содержать минимум 6 символов');
            document.querySelector('.b-authorization--form--text---reg-pass')
                .classList
                .add('b-authorization--form--text---error');

            return;
        }

        if(regular.test(formObj.email) === true && formObj.password.length >= 6) {

            ndbService.signUp(formData)
                .then(data => {
                    if(data.status === 400) {
                        showErr(data.error);
                        if(data.error === 'Этот email уже занят') {
                            document.querySelector('.b-authorization--form--text---reg-email')
                                .classList
                                .add('b-authorization--form--text---error');

                            return;
                        }
                        if(data.error === 'Этот логин уже занят') {
                            document.querySelector('.b-authorization--form--text---reg-login')
                                .classList
                                .add('b-authorization--form--text---error');

                            return;
                        }
                    }
                    if(data.status === 200) {
                        ndbService.signIn(formData)
                            .then(data => {
                                localStorage.setItem('userId', data.userId);
                                action({
                                    isLoggedIn: true,
                                    email: data.userEmail,
                                    userId: data.userId,
                                    login: data.login
                                });
                                close();
                            })
                    }
                })
        }
    }


    return (
        <React.Fragment>
            <p className="b-authorization--error-text" ref={errorRef}></p>
            <form className="b-authorization--form" onSubmit={signUpHandler}>
                <div className="b-authorization--form--field">
                    <input type="email" className="b-authorization--form--text b-authorization--form--text---reg-email" name="email" placeholder="Email" required onInput={clearErrorType}/>
                </div>
                <div className="b-authorization--form--field">
                    <input type="text" className="b-authorization--form--text b-authorization--form--text---reg-login" name="login" placeholder="Логин" required onInput={clearErrorType}/>
                </div>
                <div className="b-authorization--form--field">
                    <input type="password" className="b-authorization--form--text b-authorization--form--text---reg-pass" name="password" placeholder="Пароль" required autoComplete="true" onInput={clearErrorType}/>
                </div>
                <Button type={"submit"} title={"Зарегистрироваться"} filled={true}/>
            </form>
        </React.Fragment>
    );
}

const mapStateToProps = state => {
    return {};
}

const mapDispatchToProps = dispatch => {
    return {
        onSignIn: user => dispatch(signIn(user)),
        onClosePopup: () => dispatch(closePopUp()),
        onSetMainList: list => dispatch(setMainList(list)),
        onSetWatchedList: list => dispatch(setWatchedList(list)),
        onOpenPopup: popup => dispatch(openPopUp(popup))
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(withNDBService()(AuthorizationBlock));