import React, { Component } from 'react';
import { connect } from 'react-redux';
import { closePopUp } from '../../../actions';
import AuthorizationBlock from './authorization-block';

class AuthorizationPopup extends Component {

    componentDidMount() {

    }

    render () {
        return (
            <div className="b-authorization">
                <button type="button" className="b-authorization--close" onClick={this.props.onClosePopup}></button>
                <AuthorizationBlock type={this.props.popup.subtype}/>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        popup: state.popup
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onClosePopup: () => dispatch(closePopUp())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AuthorizationPopup)