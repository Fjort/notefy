import React, { Component } from 'react';
import YouTube from 'react-youtube';
import { connect } from 'react-redux';
import { closePopUp } from '../../../actions';

import './style.css';

class Trailer extends Component {
    state = {
        playerObj: null,
        id: null
    }

    componentDidMount() {
        const id = this.props.popup.subType;

        this.setState({id});
    }

    componentDidUpdate(prevProps, prevState) {
        if(prevProps.popup.subType !== this.props.popup.subType) {
            const id = this.props.popup.subType;

            this.setState({id});
        }
    }

    _onReady = (event) => {
        this.setState({ 
            playerObj: event.target
        });

        event.target.playVideo();
    }   

    render () {
        const { id } = this.state;
        const opts = {
            height: '390',
            width: '640',
            playerVars: {
                autoplay: 1
            }
        };

        return (
            <div className="b-trailer">
                <button type="button" className="b-trailer--close" onClick={this.props.onClosePopup}></button>
                <div className="b-trailer--video">
                    <YouTube
                        videoId={id}
                        opts={opts}
                        onReady={this._onReady}
                        className={"b-trailer--video--entity"}
                    />
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        popup: state.popup
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onClosePopup: () => dispatch(closePopUp())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Trailer);