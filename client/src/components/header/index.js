import React from 'react';
import { Link, withRouter } from 'react-router-dom';
import { openPopUp } from '../../actions';
import { connect } from 'react-redux';

import './style.css';

function Header (props) {

    const { isLoggedIn } = props;

    function openMainMenu () {
        const menu = document.querySelector('.l-main-menu');
        menu.classList.add('l-main-menu---open');
    }

    function openRegistration () {
        props.onOpenPopup({
            state: true,
            type: 'authorization',
            subtype: 'login'
        });
    }

    const search = (e) => {
        e.preventDefault();
        const form = e.target;
        const formData = new FormData(form);
        const value = formData.get('search');
        if(!value) {
            return;
        }

        props.history.push(`/search/${value}`);
    }

    if(isLoggedIn) {
        return (
            <header className="l-header">
                <section className="l-header--section">
                    <div className="l-header--section--inside">
                        <div className="b-header">
                            <nav className="b-navigation">
                                <div className="b-logo">
                                    <Link to="/" className="b-logo--link">
                                        <img src="/assets/i/logo.svg" alt="" className="b-logo--entity"/>
                                    </Link>
                                </div>
                                <form className="b-navigation--search" onSubmit={(e) => search(e)}>
                                    <input type="text" name="search" className="b-navigation--search--text" placeholder="Название фильма, сериала или имя человека"/>
                                    <button type="submit" className="b-navigation--search--submit">
                                        <img src="/assets/i/search-button-icon.svg" alt="" className="b-navigation--search--submit--icon"/>
                                    </button>
                                </form>
                                <div className="b-navigation--actions">
                                    <div className="b-navigation--actions--clause">
                                        <Link to={`/list/${props.login}`} className="b-navigation--actions--clause--link">
                                            <img src="/assets/i/list-icon.svg" alt="" className="b-navigation--actions--clause--icon"/>
                                            <span className="list-quantity">{props.quantity}</span>
                                        </Link>
                                    </div>
                                    <div className="b-navigation--actions--clause">
                                        <Link to={`/watched/${props.login}`} className="b-navigation--actions--clause--link">
                                            <img src="/assets/i/watched-list-icon.svg" alt="" className="b-navigation--actions--clause--icon"/>
                                            <span className="list-quantity">{props.watchedQuantity}</span>
                                        </Link>
                                    </div>
                                    <div className="b-navigation--actions--clause">
                                        <button className="b-navigation--actions--clause--menu" onClick={() => openMainMenu()}>
                                            <span className="b-navigation--actions--clause--menu--line"></span>
                                        </button>
                                    </div>
                                </div>
                            </nav>
                        </div>
                    </div>
                </section>
            </header>
        );
    } else {
        return (
            <header className="l-header">
                <section className="l-header--section">
                    <div className="l-header--section--inside">
                        <div className="b-header">
                            <nav className="b-navigation">
                                <div className="b-logo">
                                    <Link to="/" className="b-logo--link">
                                        <img src="/assets/i/logo.svg" alt="" className="b-logo--entity"/>
                                    </Link>
                                </div>
                                <form className="b-navigation--search" onSubmit={(e) => search(e)}>
                                    <input type="text" name="search" className="b-navigation--search--text" placeholder="Название фильма, сериала или имя человека"/>
                                    <button type="submit" className="b-navigation--search--submit">
                                        <img src="/assets/i/search-button-icon.svg" alt="" className="b-navigation--search--submit--icon"/>
                                    </button>
                                </form>
                                <div className="b-navigation--actions">
                                    <div className="b-navigation--actions--clause">
                                        <button to="/" onClick={openRegistration} className="b-navigation--actions--clause--auth">
                                            <img src="/assets/i/user-icon.svg" alt="" className="b-navigation--actions--clause--icon"/>
                                        </button>
                                    </div>
                                </div>
                            </nav>
                        </div>
                    </div>
                </section>
            </header>
        );
    }

}

const mapStateToProps = state => {
    return {}
}

const mapDispatchToProps = dispatch => {
    return {
        onOpenPopup: popup => dispatch(openPopUp(popup))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Header));