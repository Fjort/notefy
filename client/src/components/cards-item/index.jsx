import React from 'react';
import { Link } from 'react-router-dom';

import './style.css';

export default function CardsItem ({card, mediaType}) {
    const { id, picture, title, subTitle } = card;

    let type = '';
    let imagePlaceholder = '';
    const picturePath = `https://image.tmdb.org/t/p/w500${picture}`;

    switch (mediaType) {
        case 'movie':
            type = 'movie';
            imagePlaceholder = '/assets/i/card-item-image-placeholder.svg';
            break;
        case 'series':
            type = 'series';
            imagePlaceholder = '/assets/i/card-item-image-placeholder.svg';
            break;
        case 'person':
            type = 'person';
            imagePlaceholder = '/assets/i/person-img-placeholder.svg';
            break;
    
        default:
            break;
    }

    const path = `/${type}/${id}`;
    const pictureEntity = picture ? picturePath : imagePlaceholder;

    return (
        <Link to={path} className="b-cards--collection--clause">
            <div className="b-cards--collection--clause--picture">
                <img className="b-cards--collection--clause--picture--entity" src={pictureEntity} alt=""/>
            </div>
            <h5 className="b-cards--collection--clause--title">{title}</h5>
            <h6 className="b-cards--collection--clause--subtitle">{subTitle}</h6>
        </Link>
    );
}