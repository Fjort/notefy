import { createContext } from 'react';

const {
    Provider: NDBProvider,
    Consumer: NDBConsumer
} = createContext();

export {
    NDBProvider,
    NDBConsumer
}