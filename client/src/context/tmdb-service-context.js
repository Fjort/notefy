import { createContext } from 'react';

const {
    Provider: TmDBProvider,
    Consumer: TmDBConsumer
} = createContext();

export {
    TmDBProvider,
    TmDBConsumer
}