import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/app';
import { BrowserRouter as Router } from 'react-router-dom';
import { Provider } from 'react-redux';

import store from './store';
import { TmDBProvider, NDBProvider } from './context';
import { TheMovieDB, NotefyDB } from './services';

const tmdbService = new TheMovieDB();
const ndbService = new NotefyDB();

ReactDOM.render(
    <Provider store={store}>
        <NDBProvider value={ndbService}>
            <TmDBProvider value={tmdbService}>
                <Router>
                    <App />
                </Router>
            </TmDBProvider>
        </NDBProvider>
    </Provider>, 
    document.getElementById('root')
);
