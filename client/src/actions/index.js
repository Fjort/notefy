const signIn = user => {
    return {
        type: "SIGN_IN",
        payload: user
    }
}
const openPopUp = popup => {
    return {
        type: "OPEN_POPUP",
        payload: popup
    }
}

const setMainList = list => {
    return {
        type: "SET_MAIN_LIST",
        payload: list
    }
}

const setWatchedList = list => {
    return {
        type: "SET_WATCHED_LIST",
        payload: list
    }
}

const closePopUp = () => {
    return {
        type: "CLOSE_POPUP",
        payload: {
            state: false,
            type: null,
            subtype: null
        }
    }
}

const addItemToList = item => {
    return {
        type: "ADD_ITEM_TO_LIST",
        payload: item
    }
}

const changeAllList = list => {
    return {
        type: "CHANGE_ALL_LIST",
        payload: list
    }
}

const removeItemFromList = id => {
    return {
        type: "REMOVE_ITEM_FROM_LIST",
        payload: id
    }
}

const addToWatched = item => {
    return {
        type: "ADD_TO_WATCHED",
        payload: item
    }
}

const setLastAddedItem = item => {
    return {
        type: "SET_LAST_ADDED_ITEM",
        payload: item
    }
}


export {
    openPopUp,
    closePopUp,
    addItemToList,
    changeAllList,
    removeItemFromList,
    addToWatched,
    signIn,
    setMainList,
    setWatchedList,
    setLastAddedItem
}