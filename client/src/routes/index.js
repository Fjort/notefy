import React from 'react'
import { Route, Switch } from 'react-router-dom';

import HomePage from '../components/pages/home';
import ListPage from '../components/pages/list';
import WatchedPage from '../components/pages/watched';
import UserPage from '../components/pages/user-page';
import SearchPage from '../components/pages/search';
import ContentPage from '../components/pages/content';
import PersonPage from '../components/pages/person';
import RegistredHome from '../components/pages/home-registred';

const Routes = ({isLoggedIn}) => {
    if(isLoggedIn) {

        return (
            <Switch>
                <Route path="/" exact>
                    <RegistredHome />
                </Route>
                <Route path="/list/:login" exact>
                    <ListPage/>
                </Route>
                <Route path="/watched/:login" exact>
                    <WatchedPage />
                </Route>
                <Route path="/profile" exact>
                    <UserPage/>
                </Route>
                <Route path="/search" exact>
                    <SearchPage/>
                </Route>
                <Route path="/search/:title" component={SearchPage}/>
                <Route path="/movie/:id" component={ContentPage}/>
                <Route path="/series/:id" component={ContentPage}/>
                <Route path="/person/:id" component={PersonPage}/>
            </Switch>
        );
    }

    return (
        <Switch>
            <Route path="/" exact>
                <HomePage/>
            </Route>
            <Route path="/search" exact>
                    <SearchPage/>
            </Route>
            <Route path="/search/:title" component={SearchPage}/>
            <Route path="/movie/:id" component={ContentPage}/>
            <Route path="/series/:id" component={ContentPage}/>
            <Route path="/person/:id" component={PersonPage}/>
        </Switch>
    );
}

export default Routes;