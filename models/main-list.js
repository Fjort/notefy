const { Schema, model, Types } = require('mongoose');

const schema = new Schema({
    id: {type: Number, required: true},
    path: {type: String, required: true},
    title: {type: String, required: true},
    poster: {type: String},
    release: {type: String},
    description: {type: String},
    genres: {type: String}
});

module.exports = model("MainListItem", schema);